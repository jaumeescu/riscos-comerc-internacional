<!doctype html>
<html lang="es">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!--
		<meta http-equiv="refresh" content="30" >
		-->

	<!-- Bootstrap CSS -->
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	 crossorigin="anonymous">



	<link rel="stylesheet" href="./view/css/styles.css?v=4.9">
	<link rel="stylesheet" href="./view/css/styles-prodeca.css?v=4.2">
	<link rel="stylesheet" href="./view/css/overrides.css?v=1.5">

	<!-- BEGIN - Google Analytics -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-KQVGHQXMWC"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-KQVGHQXMWC');
</script>

	<title>RISCOS ACTUALS EN EL COMERÇ INTERNACIONAL DE PRODUCTES AGROALIMENTARIS. QUINS SÓN I QUÈ PODEM FER PER MINIMITZAR-LOS?</title>
</head>

<body class="bg-home idiomas index">

<!-- Page Content -->

<header id="header" role="banner">

<section class="top-bar-header">
	<div class="container">
		<div class="row">

			<div class="col-6">
				<img src="https://www.prodeca.cat/sites/default/files/logo-gencat.svg" alt="Generalitat de Catalunya - gencat.cat">
			</div>

			<div class="col-6 bar-elements">
				<div>

				</div>

			</div>

		</div>
	</div>
</section>


</header>

<div id="page-content" class="container">



<div class="row pt-4">
	<div class="col-lg-12 pt-1 pb-2 text-center">

		<!--<p class="mt-1"  style="color:#cc0000;">Benvolgudes / Benvolguts, estem tenim problemes tècnics que estem intentant resoldre. La jornada començarà a les 10:25. Gràcies i disculpin l'espera.</p>-->
		<!--
		<p class="mt-1">El dia 3 de març de 2023 a les 10.00h si accediu a aquesta pàgina podreu fer el seguiment de l'esdeveniment en directe.</p>
		<img src="./view/img/caratula-riscos-exportacio-sense-data.jpg">
	-->
			<p style="color:#cc0000;"><strong>Premeu <i>play</i> per iniciar el vídeo</strong></p>
			<div class="embed-responsive embed-responsive-16by9">
				<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/VfKJh-V7kaU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; showinfo=0" allowfullscreen></iframe>
				</div>
				<p class="mt-1">Els ponents donen <a href="#" data-toggle="modal" data-target="#consentiment"><strong>autorització</strong></a> d'ús d'imatge i veu per a fotografies i materials d'àudio de fires i esdeveniments </p>
			</div>

	<div class="col-lg-12  pt-2 text-left align-self-end">
		<p class="es"><strong>Pregunta al moderador o ponents</strong></p>
		<p class="en d-none"><strong>Ask the moderator</strong></p>

		<form class="bla" method="post">
			<div class="form-row mb-2">
				<div class="col">
					<textarea class="form-control form-control-sm" id="question" name="question" placeholder="Envia la teva pregunta.." rows="5" data-msg="Quina és la teva pregunta?" required></textarea>
				</div>
			</div>
			<div class="form-row">
				<div class="col-xs-6 col-lg-6 mb-2">
					<input type="text" class="form-control form-control-sm"  id="name" name="name" placeholder="Nom i cognoms" data-msg="Com et dius?" required>
				</div>
				<div class="col-xs-6 col-lg-6 mb-2">
				<select class="browser-default form-control form-control-sm" id="who"  name="who">
					<option selected>A qui li voleu preguntar?</option>
					<option value="Irene Guardiola">Irene Guardiola</option>
					<option value="Moderador">Moderador Prodeca</option>
				</select>
				</div>
				<div class="col-xs-6 col-lg-6 mb-2">
					<input type="text" class="form-control form-control-sm" id="company" name="company" placeholder="Empresa" data-msg="Quina és la teva empresa?" required>
				</div>
				<div class="col-xs-6 col-lg-6 mb-2">
					<input type="email" class="form-control form-control-sm" id="email" name="email" placeholder="Email" data-msg="Quin és el teu correu?" required>
				</div>
				<div class="col-xs-12 col-lg-2 mb-2">
					<button class="enviar btn btn-dark btn-sm"><span class="es">Enviar</span><span class="en d-none">Send</span></button>
					<div class="spinner-border sendLoader" role="status">
					  <span class="sr-only"><span class="es">Enviando...</span><span class="en d-none">Sending...</span></span>
					</div>
				</div>
				<div class="msg" class="col-12 bg-primary"></div>
			</div>
		</form>
		<div class="py-2"></div>
		<span class="clear"></span>

	</div>


</div>


<div id="info-tecnica" class="row">
	<div class="col-md-10 offset-md-1">
		<div class="">
			<div class="es">
				<p>Aspectes tècnics per a la correcta visualització de la sessió:</p>
				<ul>
				<li>Es recomana utilitzar els següents navegadors: Mozilla Firefox y Google Chrome.</li>
				<!--<li>Al acceder a la sesión podrá seleccionar el idioma para ver la sesión (Español o Inglés) pulsando sobre el boton <strong>"Cambiar idioma de traducción simultánea"</strong>, a continuación pulse sobre el idioma de su preferencia.</li>
				<li>Para escuchar correctamente el idioma seleccionado, se recomienda que el volumen del video esté al mínimo y el del idioma seleccionado al máximo.</li>
				--><li>Si no visualitza correctament la sessió, elimini les "cookies" i historial del seu navegador a PREFERÈNCIES.</li>
				</ul>
			</div>
			<div class="en d-none">
				<p>Technical requierements during the session:</p>
				<ul>
				<li>We recommend using the following browser: Mozilla Firefox and Google Chrome.</li>
				<!--<li>Once you are viewing the session, you could choose which language do you prefer (english or spanish) by clicking <strong>"Change simultaneous translation language"</strong> button.</li>
				<li>In order to listen properly the session, we recommend to mute the video and turn up the translation volume.</li>
				--><li>If you do not see the session correctly, please, delete the cookies and your browsing history.</li>
				</ul>
			</div>
		</div>
	</div>
</div>


</div>






	<!-- Page Content -->
	<!--
	<div id="page-content" class="container">
		<div class="row pt-2 pb-4 pt-md-4">
			<div class="col-md-3">
					<img src="https://www.prodeca.cat/sites/default/files/logo-prodeca.svg" alt="Sepa'20 On Air" width="300" />
			</div>
			<div class="col-md-9">
				<nav class="navbar navbar-expand-lg">
					<button class="navbar-toggler second-button collapsed" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav"
					 aria-expanded="false" aria-label="">
						<div class="animated-icon2">
							<span></span>
							<span></span>
							<span></span>
							<span></span>
						</div>
					</button>
					<div class="collapse navbar-collapse" id="navbarNav">

						<ul class="nav navbar-nav mb">


							<li class="nav-item" id="">
								<a href="#patrocinadors" class="nav-link">Patrocinadors</a>
							</li>
							<li class="nav-item" id="">
								<a href="#acces" class="nav-link">Accés</a>
							</li>
							<li class="nav-item" id="">
								<a href="#programa" class="nav-link">Programa</a>
							</li>



						</ul>


					</div>
				</nav>
			</div>
		</div>


	</div>
	-->




<!--
	<div>
		<style>
			.image-mobile-single {
				background-image: url(./view/img/prodeca/mapeig/pla_accio_3.jpg);
			}



			@media (min-width: 768px) {

				.image-desktop-single {
					background-image: url(./view/img/prodeca/mapeig/pla_accio_3.jpg);
				}

			}
		</style>

		<div class="paragraph paragraph--type--banner-header paragraph--view-mode--default">
			<div id="banner-header-image" class="main-banner image-desktop-single image-mobile-single" data-number-of-desktop-images="1"
			 data-number-of-mobile-images="1">
				<div class="container">
					<div class="banner-text v-align">


				  <h2>Commemoració del dia mundial de l’olivera i presentació de les accions futures en el sector de l’oli d’oliva.</h2>
				  <h3><strong>26 de novembre 2020</strong></h3>


					</div>
				</div>
			</div>

		</div>
	</div>
	-->
<!--
	<footer class="pt-4 mb-4 pb-4" id="patrocinadors">
		<div class="container">
			<div class="row">
				<div class="col-lg-4">
					<img src="./view/img/prodeca/mapeig/DepAgri.jpg" alt="Generalitat de Catalunya - Departament d'Agricultura" style="height: 80px;">
				</div>
				<div class="col-lg-4 text-left">
					<img src="https://www.prodeca.cat/sites/default/files/logo-prodeca.svg" alt="Prodeca" style="height: 30px; position: absolute; bottom: 16px;">
				</div>
				<div class="col-lg-4">
					<img src="./view/img/prodeca/mapeig/inter.png" alt="Prodeca" style="height: 50px; position: absolute; bottom: 16px;right: 20px;">
				</div>
			</div>
		</div>
	</footer>
-->


<!--
	<footer class="pt-4" id="patrocinadors">
		<section class="logosOro">
			<div class="container">
				<div class="row">
					<div class="col-lg-3">
						<p class="es">Organitzadors</p>
						<span class="sepFooter"></span>
						<div class="col">
							<a href="https://dopsiurana.com/" target="_blank">
								<img alt="DOP Siurana" src="./view/img/prodeca/olis/logoOrganitzadors.jpg" style="max-width: inherit;">
							</a>
						</div>

					</div>
					<div class="col-lg-6">
						<p class="es">Denominacions d'Origen Protegit de l'Oli de Catalunya</p>
						<span class="sepFooter"></span>
						<div class="row row-cols-lg-8X row-cols-6X">
							<div class="col">
								<a href="https://dopsiurana.com/" target="_blank">
									<img alt="DOP Siurana" src="./view/img/prodeca/olis/DOPs.jpg" style="max-width: inherit;">
								</a>
							</div>

									<div class="col">
										<a href="https://dopsiurana.com/" target="_blank">
											<img alt="DOP Siurana" src="./view/img/prodeca/olis/logoDOPSiurana.jpg">
										</a>
									</div>
									<div class="col">
										<a href="https://www.doterraalta.com/" target="_blank">
											<img alt="DOP Terra Alta" src="./view/img/prodeca/olis/logoDOPTerraAlta.jpg">
										</a>
									</div>
									<div class="col">
										<a href="http://inibsadental.com/es/" target="_blank">
											<img alt="DOP Terra Alta" src="./view/img/prodeca/olis/logoDOPEmporda.jpg">
										</a>
									</div>
									<div class="col">
										<a href="https://www.sensodyne.es/" target="_blank">
											<img alt="DOP Terra Alta" src="./view/img/prodeca/olis/logoDOPMontsia.jpg">
										</a>
									</div>
									<div class="col">
										<a href="https://www.invisalign.com" target="_blank">
											<img alt="DOP Terra Alta" src="./view/img/prodeca/olis/logoDOPGarrigues.jpg">
										</a>
									</div>
									-->
							<!--

									<div class="w-100 d-md-none"></div>

									<div class="col">
										<a href="https://www.biohorizonscamlog.com/" target="_blank">
											<img alt="Biohorizons" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoBiohorizonsOro.svg">
										</a>
									</div>
									<div class="col">
										<a href="https://misiberica.es/" target="_blank">
											<img alt="Mis" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoMisIberiaOro.svg">
										</a>
									</div>
									<div class="col">
										<a href="https://www.nobelbiocare.com/es/es/home.html" target="_blank">
											<img alt="Nobel" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoNobelOro.svg">
										</a>
									</div>
									<div class="col">
										<a href="https://www.ticareimplants.com/" target="_blank">
											<img alt="Ticare" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoTicareOro.svg">
										</a>
									</div>

						</div>
					</div>
					<div class="col-lg-3">
						<p class="es">Colaboradors</p>
						<span class="sepFooter"></span>
						<div class="col">
							<a href="https://dopsiurana.com/" target="_blank">
								<img alt="DOP Siurana" src="./view/img/prodeca/olis/logosColaboradors.jpg" style="max-width: inherit;">
							</a>
						</div>


					</div>
				</div>
			</div>
		</section>



	</footer>
-->





	<!-- Page Content -->

	<!--
	<div id="page-content" class="container">
		<div class="row pt-2 pb-4 pt-md-4" id="acces">
			<div class="main-title">
				<p>Accés al directe</p>
			</div>

			<div class="col-lg-6 offset-lg-3 text-left">

				<div class="card bg-light text-light mt-1 no-border">

					<div class="card-body">

						<form class="form-row mt-4 needs-validation loginForm" action="login-action.php" method="post" id="frmLogin">
							<div class="col-md-12 py-2">
								<label for="user" class="sr-only">
									<span class="es">Codi</span>
									<span class="en d-none">Code</span>
								</label>
								<input type="text" class="form-control" id="user" name="user" placeholder="INTRODUEIX CODI D'ACCÉS">
							</div>

							<div class="col-md-9 py-2">

								<?php
							if(isset($_GET["error"])) {
								?>
								<div class="alert alert-danger alert-dismissible fade show" role="alert">
									<span class="msg">
										<?php echo $_GET["error"]; ?>
									</span>
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<?php
							}
							?>
							</div>
							<div class="col-md-3 py-2 text-right">
								<button type="submit" name="login" value="Login" class="btn btn-dark mb-2">
									<span class="es">ACCEDIR</span>
									<span class="en d-none">ENTER</span>
								</button>
							</div>
						</form>



					</div>
				</div>
				<br/>
				<br/>

			</div>


		</div>
	</div>
	-->


	<div id="page-content" class="container">

	<section id="ponents" class="body-content section">


<div class="row pb-4">
<div class="col-lg-6 offset-lg-3 text-left">
<div class="main-title mt-4">
	<p>PONENTS</p>
</div>
			<p>
				<strong>Irene Guardiola</strong> - Advocada especialitzada en Dret duaner i Comerç exterior a Guardiola Lawyers
			</p>

</div>
<!--
<div class="col-lg-6 text-left">
<div class="main-title mt-4">
	<p>Objectius</p>
</div>


<ul>
	<li>Conèixer els canvis més importants que ha implicat el Brexit </li>
	<li>Aclarir la documentació que s’ha de tramitar </li>
	<li>Recomanacions per gestionar el canvi</li>
</ul>

<div class="main-title mt-4">
	<p>A QUI VA DIRIGIT </p>
</div>

<ul>
	<li>Directors d’empreses</li>
	<li>Responsables d’exportació</li>
	<li>Responsables de màrqueting</li>
	<li>Responsables de logística</li>
	<li>Responsables de finances</li>
	<li>Responsables de l’àmbit legal</li>
	<li>Importadors</li>
	<li>Institucions d’àmbit econòmic / empresarial </li>
</ul>
</div>
-->
</div>
</section>





	<section id="programa" class="body-content section">
			<div class="main-title mt-4">
				<p>PROGRAMA</p>
			</div>

			<div class="row pb-4">

				<div class="col-lg-8 offset-lg-2 text-left">
					<table style="width: 100%;" cellspacing="1" cellpadding="1" border="0">
						<tbody>
							<tr>
								<td class="hour">10.00 a 10.05 h</td>
								<td class="description">
									<div class="row">
										<div class="col-lg-12">
											<p><strong>Benvinguda</strong></p>
											<p><strong>Eva Castro</strong>: Àrea de Coneixement i Estretègia a Prodeca</p>
										</div>
									</div>


								</td>
							</tr>
							<tr>
								<td class="hour">10.05 a 11.30 h</td>
								<td class="description">
									<div class="row">
										<div class="col-lg-12">
										<p><strong>Irene Guardiola</strong></p>
										<p>Advocada a Guardiola Lawyers</p>
										<!--
										<ul>
											<li>Diferències amb el contracte d'agència i altres contractes</li>
											<li>Regulació</li>
											<li>Contingut</li>
											<li>Resolució del contracte</li>
										</ul>
						-->
										</div>
								</div>
								</td>
							</tr>

							<tr>
								<td class="hour">11.30 a 12 h</td>
								<td class="description">
									<p><strong>Torn obert de preguntes</strong></p>
								</td>
							</tr>
						</tbody>
					</table>
					<p><i>Moderadora: Eva Castro – Àrea de Coneixement i estratègia de Prodeca</i></p>
				</div>



			</div>




		</section>

<!--
		<section id="ponents" class="body-content section">
			<div class="row pb-4">
				<div class="col-lg-6 offset-lg-3 text-left">
				<div class="main-title mt-4">
					<p>CONTINGUT</p>
				</div>
					<ul>
					<li>Legislació a nivell espanyol i europeu sobre envasos i embalatges i els seus residus</li>
				<li>Eixos principals de l’economia circular</li>
				<li>Tendències de mercat</li>
				<li>Actuals barreres i oportunitats en el món del packaging i de l’alimentació</li>
				<li>Recomanacions sobre circularitat dels envasos</li>
					</ul>
				</div>
			</div>
		</section>
						-->

<!--
		<section id="ponents" class="body-content section">
			<div class="main-title mt-4">
				<p>A QUI VA DIRIGIT </p>
			</div>

			<div class="row pb-4">
			<ul>
				<li>Directors d’empreses</li>
				<li>Responsables d’exportació</li>
				<li>Responsables de màrqueting</li>
				<li>Responsables de logística</li>
				<li>Responsables de finances</li>
				<li>Responsables de l’àmbit legal</li>
				<li>Importadors</li>
				<li>Institucions d’àmbit econòmic / empresarial </li>
			</ul>
			</div>
		</section>
						-->

		<!--
		<div class="row mb-4">
			<div class="col-md-8 offset-md-2 text-center mt-2 mt-md-5">
				<p class="es">Si tiene dudas puede ponerse en contacto con nosotros a través de estos teléfonos mediante llamada telefónica o whatsapp.</p>
				<p class="en d-none">If you are not sure how to gain access please contact us via the following telephone numbers, either by calling or by
					WhatsApp.
				</p>
			</div>
			<div class="col-md-8 offset-md-2 mt-3 col-telf-mobile d-flex flex-wrap justify-content-around align-items-center">


				<span class="phone">
					<a href="https://wa.me/34676806854" class="whatsapp_link">
						<img src="./view/img/sepa20/whatsapp-icon.svg" class="svg-icon" />+34 676 806 854</a>
				</span>
				<span class="phone">
					<a href="https://wa.me/34669674157" class="whatsapp_link">
						<img src="./view/img/sepa20/whatsapp-icon.svg" class="svg-icon" />+34 669 674 157</a>
				</span>
				<span class="phone">
					<a href="https://wa.me/34620255105" class="whatsapp_link">
						<img src="./view/img/sepa20/whatsapp-icon.svg" class="svg-icon" />+34 620 255 105</a>
				</span>



			</div>
		</div>


-->








	</div>
	<!-- -->

	<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
	 crossorigin="anonymous"></script>

	 <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.min.js"></script>

	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
	 crossorigin="anonymous"></script>

	<script src="./view/js/scripts.js?v=4"></script>

	<script src="./view/js/main.js?v=1"></script>





	<?php
	/* ============ browser update ================  */
	//include 'browser-update.php';
	?>

	<?php
	/* ============ Cookie consent ================  */
	include 'cookie-consent.php';
	?>


</body>

</html>

