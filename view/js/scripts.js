
(function($) {

	"use strict";


	/**
	 * Envía formulario de pregunta
	 *
	 */
	$('.enviar').on('click',function(e){

		console.log("SI"+window.location.hostname);

		var $thisForm = $(this).parents('form');

		$($thisForm).validate({
			rules: {
				name: "required",
				email: {
					required: true,
					email: true
				},
				surname: "required"
			},
			errorElement: "span" ,
			submitHandler: function(form) {

				$('.msg',$thisForm).html('');
				console.log($($thisForm).serialize());

				$.ajax( {
					type: "POST",
					url:'https://'+window.location.hostname+'/riscos-comerc-internacional/send_question.php',
					dataType: 'json',
					data:$($thisForm).serialize(),

					beforeSend: function()
					{
						$('.sendLoader',$thisForm).show();
						$('.enviar',$thisForm).hide();

						console.log('beforeSend..');
					},
					success: function(result)
					{
						if(result.status == 'success'){
							console.log(result.status);

							var lng = $('html').attr('lang');
							var textoOk = lng == 'es' ? 'Gràcies, pregunta enviada' : 'Question Sent';

							$('.msg',$thisForm).html(textoOk);
							$('.msg',$thisForm).fadeIn();


						 }else if(result.status == 'error'){
							console.log('error:'+result.message);

							$('.msg',$thisForm).html(result.message);
							$('.msg',$thisForm).fadeIn();
						 }else{
							 console.log('error');
						 }
					},
					complete: function()
					{
						$('.sendLoader',$thisForm).hide();
						$($thisForm)[0].reset();
						$('.enviar',$thisForm).show();

						$('.msg',$thisForm).delay(4000).fadeOut(function(){
							$('.msg',$thisForm).html('');
						});

						console.log('complete!');
					},
					error: function(result)
					{
						alert('ajax error! \n '+JSON.stringify(result));
						console.log(JSON.stringify(result));
					},
				});
			}
		});
	});


	/* Cambio de idioma */
	$('a','#lang-selector').on('click',function(e){
		e.preventDefault();

		var lang = $(this).data('lang');
		cambiaIdioma(lang);
	});



	function cambiaIdioma(lang){

		$('.idiomas .es,.idiomas .en').toggleClass('d-none');
		$('#lang-selector a').toggleClass('active');

		$('html').attr('lang',lang);

		localStorage.setItem("idioma", lang);



		//dashboard
		if( $('.contactForm').length > 0 ){

			var txtarea = "Envía tu pregunta..";
			var txt  = "Nombre + Apellidos";
			var txtareaErr = "¿Cuál es tu pregunta?";
			var txtErr = "Aquí falta tu nombre";
			var emailErr = "¿Tu email?";


			if (lang=='en'){
				txtarea = "Send your question..";
				txt  = "Name + Surname";
				txtErr = "Please fill this field";
				txtareaErr = "Please fill this field";
				emailErr ="Please fill this field";
			}

			var formulario = $('form')[0];
			var $textArea = $('textarea',formulario)[0];
				$textArea.placeholder = txtarea;
				$($textArea).data('msg',txtareaErr);
			var $textInput = $('input[type=text]',formulario)[0];
				$textInput.placeholder = txt;
				$($textInput).data('msg',txtErr);
			var $emailInput = $('input[type=email]',formulario)[0];
				$($emailInput).data('msg',emailErr);
		}

		// login
		if ( $('.loginForm').length > 0 ){

			var user = "INTRODUCE USUARIO";
			var pass = "INTRODUCE CONTRASEÑA";

			if (lang=='en'){
				user = "ENTER USERNAME";
				pass = "ENTER PASSWORD";
			}

			var formulario = $('form')[0];
			var $userInput = $('input[name=user]',formulario)[0];
				$userInput.placeholder = user;;
			var $passInput = $('input[name=password]',formulario)[0];
				$passInput.placeholder = pass;

			var msgErr = $('.alert .msg').html();
			if (  msgErr == 'Usuario y/o contraseña incorrectos.' && lang == 'en' ){
				$('.alert .msg').html('Wrong username or password.');
			}

		}





	}


})(jQuery);




$(function() {

	// idioma
   	var idioma = "es";
	var currentLang = idioma;
	if( localStorage.getItem("idioma") ){
		currentLang = localStorage.getItem("idioma");
	}
	if( currentLang == "en"){
		$('a[data-lang="en"]','#lang-selector').trigger('click');
	}

	// validate form login
	$('#frmLogin').submit(function() {
	if ($.trim($("#user").val()) === "") {
			//empty field
			return false;
		}
	});



});










