<?php
	/** Cookie consent
	* https://html-online.com/articles/cookie-warning-widget-with-javascript/
	*/
	
?>

<!-- =============== Cookie consent ============= -->
<style>
#myCookieConsent {
    z-index: 999;
    min-height: 20px;
    position: fixed;
    bottom: 0;
	left:0;
    right: 0;
    display: none;
	background-color:#ff0000;
	color:white;
	line-height: 1.2em;
}
.btn-link{
	text-decoration:underline!important;
}
</style>

<div id="myCookieConsent" class="py-2">
	<div class="container">
        <div class="row">
			<div class="col-8 text-left">Les galetes d'aquest lloc web, no recopilen cap informació personal. S'utilitzen únicament per analitzar el trànsit i compartim aquesta informació amb els nostres partners.</div>
			<div class="col-4 text-right"><a id="cookieButton" class="btn btn-dark">Entesos</a></div>
		</div>
	</div>
</div>



<script>
	// Cookie Compliancy BEGIN
	function GetCookie(name) {
	  var arg=name+"=";
	  var alen=arg.length;
	  var clen=document.cookie.length;
	  var i=0;
	  while (i<clen) {
		var j=i+alen;
		if (document.cookie.substring(i,j)==arg)
		  return "here";
		i=document.cookie.indexOf(" ",i)+1;
		if (i==0) break;
	  }
	  return null;
	}
	function testFirstCookie(){
		var offset = new Date().getTimezoneOffset();
		if ((offset >= -180) && (offset <= 0)) { // European time zones
			var visit=GetCookie("cookieCompliancyAccepted");
			if (visit==null){
			   $("#myCookieConsent").fadeIn(400);	// Show warning
		   } else {
				// Already accepted
		   }		
		}
	}
	$(document).ready(function(){
		$("#cookieButton").click(function(){
			//console.log('Understood');
			var expire=new Date();
			expire=new Date(expire.getTime()+7776000000);
			document.cookie="cookieCompliancyAccepted=here; expires="+expire+";path=/";
			$("#myCookieConsent").hide(400);
		});
		testFirstCookie();
	});
	// Cookie Compliancy END
</script>

<!-- //=============== Cookie consent ============= -->


<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="consentiment" aria-hidden="true" id="consentiment">
  <div class="modal-dialog modal-lg">
  <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Autorització d'ús d'imatge i veu per a fotografies i materials d'àudio de fires i esdeveniments</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  <p>Per la present, la persona signant <strong>AUTORITZA</strong>:</p>
	  <p>A PROMOTORA D'EXPORTACIONS CATALANES, SA (En endavant, Prodeca), amb domicili social en Carrer Dr. Roux 80, 08017 (Barcelona) i NIF A58241316, per a la captació, ús, divulgació i publicació, reproducció, distribució i transformació d'imatge i / o veu en diversos mitjans tant digitals, inclòs internet, com en paper (com revistes, pàgines web o xarxes socials).</p>
	  <p>Mitjançant l'atorgament de la present autorització, la persona signant CONSENTEIX de forma clara i explícita el tractament de les dades, imatge i / o veu, per a les finalitats descrites en l'apartat anterior.</p>
	  <p>En el cas que es tracti de dades personals referides a menors d'edat inferiors a 14 anys, la persona signant CONSENTEIX, com a titular de la pàtria potestat o tutela sobre el nen, de manera clara i explícita el tractament de les dades, imatge i / o veu, per a les finalitats descrites en apartats anteriors.</p>
	  <p>El signant queda informat que el responsable del tractament de les dades proporcionades és Promotora d'Exportacions Catalanes, SA, amb domicili en Carrer Dr. Roux 80, 08017 (Barcelona) amb NIF A58241316 i número de telèfon 935 52 48 20.</p>
	  <p><strong>Base legítima del tractament</strong></p>
	  <p>Tractarem les seves dades sobre la base del consentiment atorgat i d'acord amb la present autorització</p>
	  <p><strong>Drets de l'interessat i conservació de les dades</strong></p>
	  <ul>
	  <li>Vostè pot exercir els seus drets d'accés, de rectificació, oposició, supressió, de limitació i portabilitat. Per a això, enviï un correu a l'adreça de correu electrònic contacte@prodeca.cat, identificant clarament el motiu de la seva sol·licitud i la seva identitat o bé dirigint les seves comunicacions per escrit a la següent adreça: Carrer Dr. Roux 80, 08017 (Barcelona).</li>
	  <li>Té dret a revocar el consentiment per a l'ús i tractament de les seves fotografies i materials d'àudio en qualsevol moment a través dels mitjans indicats en el paràgraf anterior. La revocació del consentiment no afecta la legalitat del tractament de dades dut a terme fins al moment de la revocació.</li>
	  <li>Tractarem les seves dades mentre vostè no revoqui el consentiment i fins a les prescripcions legalment establertes.</li>
	  <li>No cedirem les seves dades a tercers països ni a organitzacions internacionals, excepte exigència legal.</li>
	  <li>Així mateix, té dret a recórrer davant les autoritats reguladores en matèria de protecció de dades (AEPD).</li>
	  <li>Pot posar-se en contacte amb el delegat de Protecció de Dades a través del correu electrònic contacte@prodeca.cat.</li>
	  </ul>

      </div>
	  <!--
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Send message</button>
      </div>
	  -->
    </div>


  </div>
</div>