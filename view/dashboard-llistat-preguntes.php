<?php
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header('Content-Type: text/html; charset=utf-8');

?>


<!doctype html>
<html lang="es">
<head>
<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet"
		integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

	<link rel="stylesheet" href="css/styles.css?v=4.1">
	<link rel="stylesheet" href="css/styles-prodeca.css?v=4.2">
	<link rel="stylesheet" href="css/overrides.css">


    <title>Seminari en línia - ASPECTES BÀSICS DEL CONTRACTE DE DISTRIBUCIÓ INTERNACIONAL EN EL SECTOR DE L'ALIMENTACIÓ</title>

	<!-- BEGIN - Google Analytics -->
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-XXXXXX-X"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-XXXXXX-X');
	</script>
	<!-- END - Google Analytics -->


</head>

<body class="bg-home isLoggedIn idiomas">

<?php

//include_once ("../MeetingValidation.php");

use \Phppot\Member;
require_once ("../class/Member.php");

//$mv = new MeetingValidation();
$member = new Member();


//$mv->getUser();
//$userdata = $mv->getUserBasics();
//$userdata[] = basename($_SERVER['PHP_SELF'],'.php');
$questionsResult = $member->getQuestions();



?>



    <!-- Page Content -->
    <div id="page-content" class="container">


		<div class="row py-4">
			<div class="col-4 col-md-2 text-left">
			<img src="https://www.prodeca.cat/sites/default/files/logo-prodeca.svg" alt="Prodeca" width="220"  class="mt-3 mt-md-2"/>
			</div>
			<div class="col-4 col-md-8 text-center">
				<h1>Seminari en línia - ASPECTES BÀSICS DEL CONTRACTE DE DISTRIBUCIÓ INTERNACIONAL EN EL SECTOR DE L'ALIMENTACIÓ</h1>
			</div>

			<div class="col-4 col-md-2 text-right">
				<!--
				<div id="lang-selector" class="text-right">
					<a href="#" class="btn btn-outline-dark active" data-lang="es">ES</a>
					<a href="#" class="btn btn-outline-dark" data-lang="en">EN</a>
				</div>
-->
				<div class="cerrar-sesion text-right">
					<a href="/logout.php" class="btn Xbtn-dark"> <span aria-hidden="true">&times;</span> <span class="es">Sortir</span><span class="en d-none">Log out</span></a>
				</div>
			</div>
		</div>



		<!-- Translation
		<div class="col-lg-8 offset-lg-2 text-right pb-2">

			<button type="button" class="btn btn-outline-dark btn-sm" data-toggle="collapse" data-target="#iFrameTransContainer" aria-expanded="false" aria-controls="iFrameTransContainer">
				<svg width="1.4em" height="1.4em" viewBox="0 0 16 16" class="bi bi-chat-quote" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
				  <path fill-rule="evenodd" d="M2.678 11.894a1 1 0 0 1 .287.801 10.97 10.97 0 0 1-.398 2c1.395-.323 2.247-.697 2.634-.893a1 1 0 0 1 .71-.074A8.06 8.06 0 0 0 8 14c3.996 0 7-2.807 7-6 0-3.192-3.004-6-7-6S1 4.808 1 8c0 1.468.617 2.83 1.678 3.894zm-.493 3.905a21.682 21.682 0 0 1-.713.129c-.2.032-.352-.176-.273-.362a9.68 9.68 0 0 0 .244-.637l.003-.01c.248-.72.45-1.548.524-2.319C.743 11.37 0 9.76 0 8c0-3.866 3.582-7 8-7s8 3.134 8 7-3.582 7-8 7a9.06 9.06 0 0 1-2.347-.306c-.52.263-1.639.742-3.468 1.105z"/>
				  <path d="M7.468 7.667c0 .92-.776 1.666-1.734 1.666S4 8.587 4 7.667C4 6.747 4.776 6 5.734 6s1.734.746 1.734 1.667z"/>
				  <path fill-rule="evenodd" d="M6.157 6.936a.438.438 0 0 1-.56.293.413.413 0 0 1-.274-.527c.08-.23.23-.44.477-.546a.891.891 0 0 1 .698.014c.387.16.72.545.923.997.428.948.393 2.377-.942 3.706a.446.446 0 0 1-.612.01.405.405 0 0 1-.011-.59c1.093-1.087 1.058-2.158.77-2.794-.152-.336-.354-.514-.47-.563zm-.035-.012h-.001.001z"/>
				  <path d="M11.803 7.667c0 .92-.776 1.666-1.734 1.666-.957 0-1.734-.746-1.734-1.666 0-.92.777-1.667 1.734-1.667.958 0 1.734.746 1.734 1.667z"/>
				  <path fill-rule="evenodd" d="M10.492 6.936a.438.438 0 0 1-.56.293.413.413 0 0 1-.274-.527c.08-.23.23-.44.477-.546a.891.891 0 0 1 .698.014c.387.16.72.545.924.997.428.948.392 2.377-.942 3.706a.446.446 0 0 1-.613.01.405.405 0 0 1-.011-.59c1.093-1.087 1.058-2.158.77-2.794-.152-.336-.354-.514-.469-.563zm-.034-.012h-.002.002z"/>
				</svg> <span style="vertical-align:middle" class="es"> Cambiar idioma de traducción simultánea </span> <span style="vertical-align:middle" class="en d-none"> Change simultaneous translation language</span>
			</button>
		</div>


		<div class="col-lg-12 collapse" id="iFrameTransContainer">
			<iframe class="" id="iFrameTrans" src="https://tgp.theglobalpassword.com/webapp/a1s0ro8bce5t73825u236p" frameborder="0" width="100%"></iframe>
		</div>
		<!-- -->




    </div>



	<div id="page-content" class="container">

		<section id="programa" class="body-content section">
			<div class="main-title">
				<p>LLISTAT DE PREGUNTES</p>
			</div>

			<div class="row pb-4">

				<div class="col-lg-12 text-left">
					<table style="width: 100%;" cellspacing="1" cellpadding="1" border="0">
						<tbody>
							<tr>
								<th>Nom</th>
								<th>Empresa</th>
								<th>Ponent</th>
								<th>Email</th>
								<th>Pregunta</th>
							</tr>
							<?php
								foreach($questionsResult as $question) {
							?>
							<tr>
								<td class="hour" width="100"><?php echo $question["name"];?></td>
								<td class="hour" width="100"><?php echo $question["company"];?></td>
								<td class="hour" width="100"><?php echo $question["who"];?></td>
								<td class="hour" width="100"><?php echo $question["email"];?></td>
								<td class="description">
									<?php echo $question["question"];?>
								</td>
							</tr>

							<?php
								}
							?>

						</tbody>
					</table>
				</div>



			</div>




		</section>


		<!--
		<div class="row mb-4">
			<div class="col-md-8 offset-md-2 text-center mt-2 mt-md-5">
				<p class="es">Si tiene dudas puede ponerse en contacto con nosotros a través de estos teléfonos mediante llamada telefónica o whatsapp.</p>
				<p class="en d-none">If you are not sure how to gain access please contact us via the following telephone numbers, either by calling or by
					WhatsApp.
				</p>
			</div>
			<div class="col-md-8 offset-md-2 mt-3 col-telf-mobile d-flex flex-wrap justify-content-around align-items-center">


				<span class="phone">
					<a href="https://wa.me/34676806854" class="whatsapp_link">
						<img src="./view/img/sepa20/whatsapp-icon.svg" class="svg-icon" />+34 676 806 854</a>
				</span>
				<span class="phone">
					<a href="https://wa.me/34669674157" class="whatsapp_link">
						<img src="./view/img/sepa20/whatsapp-icon.svg" class="svg-icon" />+34 669 674 157</a>
				</span>
				<span class="phone">
					<a href="https://wa.me/34620255105" class="whatsapp_link">
						<img src="./view/img/sepa20/whatsapp-icon.svg" class="svg-icon" />+34 620 255 105</a>
				</span>



			</div>
		</div>


-->



		<footer class="pt-4">

		</footer>


	</div>
	<!-- -->




<!--
	<div class="row">
		<div class="col text-center py-2">
			<a href="https://sepa.6connex.eu/event/sepaonair/" target="_blank" class="visita-nuestro-stand"><img src="img/sepa20/visita-nuestro-stand.svg"/></a>
		</div>
	</div>
-->
<!--
    <div id="footer" class="pt-0">
        <div class="row py-4">
            <div class="col-md-12 Xoffset-md-2 text-center">
             <p class="es">Si tiene dudas puede ponerse en contacto con nosotros a través de estos teléfonos mediante llamada telefónica o whatsapp.</p>
			 <p class="en d-none">If you are not sure how to gain access please contact us via the following telephone numbers, either by calling or by WhatsApp.</p>
            </div>
            <div class="col-md-8 offset-md-2 Xmt-3 col-telf-mobile d-flex flex-wrap justify-content-around align-items-center">

                <span class="phone"><img src="img/sepa20/whatsapp-icon.svg" class="svg-icon"/>+34 676 80 68 54</span>
                <span class="phone"><img src="img/sepa20/whatsapp-icon.svg" class="svg-icon"/>+34 648 18 20 62</span>
				<span class="phone"><img src="img/sepa20/whatsapp-icon.svg" class="svg-icon"/>+34 669 67 41 57</span>


            </div>
        </div>
    </div>
-->
	<div id="info-tecnica" class="row">
		<div class="col-md-10 offset-md-1">
			<div class="">
				<div class="es">
					<p>Aspectes tècnics per a la correcta visualització de la sessió:</p>
					<ul>
					<li>Es recomana utilitzar els següents navegadors: Mozilla Firefox y Google Chrome.</li>
					<!--<li>Al acceder a la sesión podrá seleccionar el idioma para ver la sesión (Español o Inglés) pulsando sobre el boton <strong>"Cambiar idioma de traducción simultánea"</strong>, a continuación pulse sobre el idioma de su preferencia.</li>
					<li>Para escuchar correctamente el idioma seleccionado, se recomienda que el volumen del video esté al mínimo y el del idioma seleccionado al máximo.</li>
					--><li>Si no visualitza correctament la sessió, elimini les "cookies" i historial del seu navegador a PREFERÈNCIES.</li>
					</ul>
				</div>
				<div class="en d-none">
					<p>Technical requierements during the session:</p>
					<ul>
					<li>We recommend using the following browser: Mozilla Firefox and Google Chrome.</li>
					<!--<li>Once you are viewing the session, you could choose which language do you prefer (english or spanish) by clicking <strong>"Change simultaneous translation language"</strong> button.</li>
					<li>In order to listen properly the session, we recommend to mute the video and turn up the translation volume.</li>
					--><li>If you do not see the session correctly, please, delete the cookies and your browsing history.</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<!--
	<footer class="pt-4">
		<section class="logosPlatino">
			<div class="container">
				<p class="es">Colaboradores Estratégicos Platino</p>
				<p class="en d-none">Platinum Strategic Partners</p>

				<span class="sepFooter"></span>
				<div class="row col-lg-8X row-cols-6X">
					<div class="col">
						<a href="https://www.dentaid.com/es/" target="_blank"><img alt="Dentaid" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoDentaidPlatino.svg"></a>
					</div>
					<div class="col">
						<a href="https://www.listerine.es/hcp" target="_blank"><img alt="Listerine" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoListerinePlatino.svg"></a>
					</div>
					<div class="col">
						<a href="https://www.dentalcare.es/es-es" target="_blank"><img alt="Oral-B" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoOralbPlatino.svg"></a>
					</div>
					<div class="col">
						<a href="https://professional.sunstargum.com/es/" target="_blank"><img alt="Sunstar" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoSunstarPlatino.svg"></a>
					</div>

					<div class="w-100 d-md-none"></div>

					<div class="col">
						<a href="https://www.dentsplysirona.com/es-ib/productos/implantes.html" target="_blank"><img alt="Dentsply" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoDentsplyPlatino.svg"></a>
					</div>
					<div class="col">
						<a href="http://www.klockner.es/" target="_blank"><img alt="Klockner" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoKlocknerPlatino.svg"></a>
					</div>
					<div class="col">
						<a href="https://www.straumann.com/es/es/profesionales-de-la-odontologia.html" target="_blank"><img alt="Straumann" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoStraumannPlatino.svg"></a>
					</div>
					<div class="col">
						<a href="https://www.sweden-martina.com/" target="_blank"><img alt="Sweden-Martina" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoSwedenPlatino.svg"></a>
					</div>
				</div>
			</div>
		</section>
		<section class="logosOro">
			<div class="container">
				<p class="es">Colaboradores Estratégicos Oro</p>
				<p class="en d-none">Gold Strategic Partners</p>
				<span class="sepFooter"></span>
				<div class="row row-cols-lg-8X row-cols-6X">
					<div class="col">
						<a href="https://www.isdin.com/higiene-dental/" target="_blank"><img alt="Bexident" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoBexidentOro.svg"></a>
					</div>
					<div class="col">
						<a href="https://www.colgateprofesional.es/" target="_blank"><img alt="Colgate" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoColgateOro.svg"></a>
					</div>
					<div class="col">
						<a href="http://inibsadental.com/es/" target="_blank"><img alt="Inibsa Geistlich" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoInibsaOro.svg"></a>
					</div>
					<div class="col">
						<a href="https://www.sensodyne.es/" target="_blank"><img alt="Sensodyne" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoSensodyneOro.svg"></a>
					</div>

					<div class="col col-invisalign">
						<a href="https://www.invisalign.com" target="_blank"><img alt="Invisisalign" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoInvisisalignOro.svg"></a>
					</div>

					<div class="w-100 d-md-none"></div>

					<div class="col">
						<a href="https://www.biohorizonscamlog.com/" target="_blank"><img alt="Biohorizons" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoBiohorizonsOro.svg"></a>
					</div>
					<div class="col">
						<a href="https://misiberica.es/" target="_blank"><img alt="Mis" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoMisIberiaOro.svg"></a>
					</div>
					<div class="col">
						<a href="https://www.nobelbiocare.com/es/es/home.html" target="_blank"><img alt="Nobel" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoNobelOro.svg"></a>
					</div>
					<div class="col">
						<a href="https://www.ticareimplants.com/" target="_blank"><img alt="Ticare" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoTicareOro.svg"></a>
					</div>
				</div>
			</div>
		</section>
		<section class="logosPartners">
			<div class="container">
				<p>Media Partners</p>
				<span class="sepFooter"></span>
				<div class="row row-cols-lg-8X row-cols-6X">
					<div class="col">
						<a href="https://es.dental-tribune.com/" target="_blank"><img alt="Dental Tribune" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoDentalTribune.svg"></a>
					</div>
					<div class="col">
						<a href="http://www.eldentistamoderno.com/" target="_blank"><img alt="DM" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoDm.svg"></a>
					</div>
					<div class="col">
						<a href="https://gacetadental.com/" target="_blank"><img alt="Gaceta Dental" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoGacetaD.svg"></a>
					</div>
					<div class="col">
						<a href="https://www.odontologosdehoy.com/" target="_blank"><img alt="Odontólogos Hoy" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoOdontologosHoy.svg"></a>
					</div>

					<div class="w-100 d-md-none"></div>

					<div class="col">
						<a href="http://www.quintpub.com/" target="_blank"><img alt="Quintessence Publishing" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoQp.svg"></a>
					</div>
					<div class="col">
						<a href="https://www.quintessence.es/" target="_blank"><img alt="Quintessence Publishing España" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoQpEsp.svg"></a>
					</div>
					<div class="col">
						<a href="https://www.maxillaris.com/" target="_blank"><img alt="Maxillaris" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoMaxillaris.svg"></a>
					</div>

				</div>
			</div>
		</section>
		<section class="logoFooter">
			<span class="sepFooter"></span>
			<div class="container">
				<div class="row">
					<div class="col-4">
						<img alt="Sepa" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoSepaPerodoncia.svg">
					</div>
					<div class="col-4">
						<img alt="Congreso Bucal" src="https://sepa2020.es/wp-content/themes/sepatv/images/logo20.svg">
					</div>

					<div class="col-4">
						<img alt="Sepa" src="https://sepa2020.es/wp-content/themes/sepatv/images/logoSepaTv.svg">
					</div>
				</div>
			</div>
		</section>

	</footer>
	-->

	</div>
    <!-- -->

    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
		  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

	<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.min.js"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
		integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

    <script src="js/scripts.js?v=5"></script>



   <?php
	/* ============ browser update ================  */
	//include 'browser-update.php';
	?>

   <?php
	/* ============ Cookie consent ================  */
	include 'cookie-consent.php';
	?>

</body>

</html>
