<?php
/* Avisos y errores */

//error_reporting(E_ALL);											// DESARROLLO EXTRICTO 
//error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED ); 				// PRUEBAS
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_WARNING );		// PRODUCCION


/* Fechas y horas */

date_default_timezone_set ('Europe/Madrid');
setlocale(LC_ALL,"es_ES");

?>