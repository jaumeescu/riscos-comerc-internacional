<?php
namespace Phppot;

use \Phppot\DataSource;

class Member
{

    private $dbConn;

    private $ds;

    function __construct()
    {
        require_once "DataSource.php";
        $this->ds = new DataSource();
    }

    public function tester() {
        return "Incorrecto";
    }

    public function validateUserCode($userCode) {
        $query = "select * FROM users WHERE code = ?";
        $paramType = "s";
        $paramArray = array($userCode);
        $memberResult = $this->ds->select($query, $paramType, $paramArray);

        return $memberResult;
    }

    public function createUser($name,$surname,$email,$profile,$code) {
        $code = $this->generateRandomString();
        $query = "INSERT INTO users (id,name,surname,email,profile,code) VALUES (NULL,?,?,?,?,?)";
        $paramType = "sssss";
        $userdata = array();
        $userdata[] = $name;
        $userdata[] = $surname;
        $userdata[] = $email;
        $userdata[] = $profile;
        $userdata[] = $code;
        $paramArray = $userdata;
        $storedResult = $this->ds->insert($query, $paramType, $paramArray);

        return $code;
    }

    public function createQuestion($question,$name,$who,$company,$email) {
        $query = "INSERT INTO questions_riscos_comerc_internacional (id,question,name,who,company,email) VALUES (NULL,?,?,?,?,?)";
        $paramType = "sssss";
        $questiondata = array();
        $questiondata[] = $question;
        $questiondata[] = $name;
        $questiondata[] = $who;
        $questiondata[] = $company;
        $questiondata[] = $email;
        $paramArray = $questiondata;
        $storedResult = $this->ds->insert($query, $paramType, $paramArray);
    }

    public function getQuestions() {
        $query = "select * FROM questions_riscos_comerc_internacional";
        $questionsResult = $this->ds->select($query);

        return $questionsResult;
    }


    public function generateRandomString($length = 8) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function getMemberById($memberId)
    {
        $query = "select * FROM users WHERE id = ?";
        $paramType = "i";
        $paramArray = array($memberId);
        $memberResult = $this->ds->select($query, $paramType, $paramArray);

        return $memberResult;
    }

    public function processLogin($username, $password) {
        $passwordHash = md5($password);
        $query = "select * FROM users WHERE username = ? AND password = ?";
        $paramType = "ss";
        $paramArray = array($username, $passwordHash);
        $memberResult = $this->ds->select($query, $paramType, $paramArray);
        if(!empty($memberResult)) {
            $_SESSION["userId"] = $memberResult[0]["id"];

            $storedResult = $this->storeLastLogin( $memberResult[0]["id"] );
            if(!empty($storedResult)) {
                return true;
            }
        }
    }

    public function storeLastLogin($userid) {
        $query = "UPDATE users SET last_login = NOW() WHERE id = ? ";
        $paramType = "i";
        $paramArray = array($userid);
        $storedResult = $this->ds->update($query, $paramType, $paramArray);
        if(!empty($storedResult)) {
            return true;
        }
    }

    public function storeUser($userdata=array()) {
        $query = "INSERT INTO users (id,iduser,fullname,email,last_login,extra) VALUES (NULL,?,?,?,NOW(),?)";
        $paramType = "isss";
        $paramArray = $userdata;
        $storedResult = $this->ds->insert($query, $paramType, $paramArray);

        return $storedResult;
    }



}
